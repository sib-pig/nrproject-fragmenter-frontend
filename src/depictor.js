$(function(){


    function checkNotEmpty(id) {
        var target=$("#"+id);
        if(target.val() == null || target.val().length==0 || /^\s+$/.test(target.val())){
            target.addClass("is-invalid");
            target.parent().children("label").addClass("text-danger");
            return false;
        }else {
            target.removeClass("is-invalid");
            target.parent().children("label").removeClass("text-danger");
            return true;
        }
    }

    $(function () {
        $('[data-toggle="popover"]').popover()
    });

    $("#customized").click(function () {
        $("#collapseExample").slideDown();

    });
    $("#default").click(function () {
        $("#collapseExample").slideUp();
    });

    $("#defaultCheck1").click(function(e){
            var currentState=$("#norineGraph").prop("disabled");
            $("#norineGraph").prop("disabled",!currentState);

            //console.log($("#norineGraph").prop("disabled"));
        });

    $("#clearButton").click(function(e){
        $('form')[0].reset();
        if (!$("#norineGraph").prop("disabled")) {
            $("#norineGraph").prop("disabled",true);
        }
    });

    $("#exampleButton").click(function(e){
       $("#smiles").val("CC1C(C(CC(O1)OC2C(C(C(OC2OC3=C4C=C5C=C3OC6=C(C=C(C=C6)[C@H]([C@H](C(=O)N[C@H](C(=O)NC5C(=O)N[C@@H]7C8=CC(=C(C=C8)O)C9=C(C=C(C=C9C(NC(=O)[C@H]([C@@H](C1=CC(=C(O4)C=C1)Cl)O)NC7=O)C(=O)O)O)O)CC(=O)N)NC(=O)[C@@H](CC(C)C)NC)O)Cl)CO)O)O)(C)N)O");
       $("#name").val("Vancomycin");

        if ($("#norineGraph").prop("disabled")) {
            document.getElementById('defaultCheck1').click();
        }
       $("#norineGraph").val("Asn,bOH-Cl-Tyr,NMe-Leu,Hpg,D-Glc,Van,bOH-Cl-Tyr,Dhpg,Hpg @1,3 @0,2,3 @1 @1,0,4,6,8 @3,5 @4 @3,7,8 @6,8 @3,7,6");
    });

        $("#submitButton").click(function(e){
            d3.select(".errorMessage").style("visibility","hidden");
            e.preventDefault();
            var allValid=true;
            if(!checkNotEmpty("name")){
                allValid=false
            }
            if(!checkNotEmpty("smiles")){
                allValid=false
            }
            if(!$("#norineGraph").prop("disabled")){
                if(!checkNotEmpty("norineGraph")){
                    allValid=false
                }
            }

            if(allValid){

                var Form={
                    name: $("#name").val(),
                    smiles: $("#smiles").val(),
                    norineGraph: $("#norineGraph").val()
                };
                var progress=d3.select(".progress").select(".progress-bar");

                $.ajax({
                    type: "POST",
                    url: "http://localhost:3128/webapi/results",
                    data: JSON.stringify(Form),
                    contentType: 'text/plain',
                    dataType: "json",


                    beforeSend: function () {
                        // Show image container

                        d3.select(".progress").style("visibility", "visible");
                        progress.transition()
                            .style("width", 20 + "%")
                            .attr("aria-valuenow", 20);

                    },

                    complete: function () {
                        progress.transition()
                            .style("width", 100 + "%")
                            .attr("aria-valuenow", 100);

                        d3.select(".progress").transition().delay(1000).duration(1000).style("visibility", "hidden")
                            .select(".progress-bar")
                            .style("width", 0 + "%")
                            .attr("aria-valuenow", 0);


                    },

                    success: function (results) {

                        console.log(results);

                        d3.select("thead tr").style("visibility","visible");
                        d3.select("#downloadSvg").attr("value",results.id);
                        d3.select("#downloadPng").attr("value",results.id);


                        d3.select("svg").remove();

                        d3.select("#resultsComp")
                            .attr("value", results.id);

                        d3.select("#coverage")
                            .attr("value", results.coverage);

                        if(!$("#norineGraph").prop("disabled")){
                            d3.select("#correctness")
                                .attr("value", results.correctness);
                        }else{
                            d3.select("#correctness")
                                .attr("value", "");
                        }

                        var color = d3.scale.category10();

                        /*
                         var margin = {top: -5, right: 0, bottom: -5, left: 0};
                         var width = 700- margin.left - margin.right,//960
                             height = 450- margin.top - margin.bottom;//500
                        */

                        var width = "100%";
                        var height = "100%";//500
                        /*

                        var zoom = d3.behavior.zoom()
                            .scaleExtent([0.5, 10])
                            .on("zoom", zoomed);

                        */
                        var zoom = d3.behavior.zoom().scaleExtent([0.5, 10]).on("zoom", zoomed);


                        var svg = d3.select("#chart").append("svg")
                            .attr("width", width)
                            .attr("height", height)
                            .style("cursor", "pointer")
                            .append("g")
                            //.attr("transform", "translate(5%,5%)")
                            .call(zoom);

                        var rect = svg.append("rect")
                            .attr("width", width)
                            .attr("height", height)
                            .style("fill", "none")
                            .style("pointer-events", "all");


                        var container = svg.append("g");

                        var force = d3.layout.force()//add charge???
                            .size([width + "5%" , height + "5%" ]);

                        var drag = force.drag()
                            .on("dragstart", dragstarted);

                        var graph=results.atomicGraph.atomicGraph;
                        /*
                                        var atomsMap = new Map();
                                        graph.atoms.forEach( function(d,i) {
                                            atomsMap.set(d.atomID,i);
                                        });
                        */
                        var clientWidth=document.getElementsByTagName('svg')[0].parentNode.clientWidth;
                        var clientHeight=document.getElementsByTagName('svg')[0].parentNode.clientHeight;

                        //console.log(clientWidth);
                        //console.log(clientHeight);
                        var centerCoordinateX=clientWidth/2;
                        var centerCoordinateY=clientHeight/2;
                        var nodes = graph.atoms.map(function(d){
                            return {
                                'index' : d.cdk_idx,
                                'name':d.name,
                                'x' : (d.x*10) + centerCoordinateX,
                                'y' : (d.y*10) + centerCoordinateY,
                                'fixed': true,
                                'res':d.res,
                                'matchIdx':d.matchIdx,
                                'monName':d.monName,
                                'labelRes': "NA"
                            }

                        });

                        var xMed = d3.median(nodes, function(d) { return d.x; });
                        var yMed = d3.median(nodes, function(d) { return d.y; });

                        var groups = d3.nest().key(function(d) { return d.matchIdx; }).entries(nodes);

                        groups.forEach(function (d) {
                            var monomerPoints=[];

                            d.values.forEach(function (d) {
                                var point=(Math.abs(xMed-d.x))+(Math.abs(yMed-d.y));
                                monomerPoints.push(point);
                            });

                            var selectedNode=d.values[monomerPoints.indexOf(d3.max(monomerPoints))];
                            selectedNode.labelRes = xMed - selectedNode.x > 0 ? "end" : "start";
                        });

                        var groupPath = function(d) {
                            return "M" +
                                d3.geom.hull(d.values.map(function(i) { return [i.x, i.y]; }))
                                    .join("L")
                                + "Z";
                        };

                        var links = graph.bonds.map(function(d){
                            return {
                                'source': d.atoms[0],
                                'target': d.atoms[1],
                                'arity':d.arity,
                                'res':d.res
                            }
                        });
                        force
                            .nodes(nodes)
                            .links(links)
                            .on("tick", tick)
                            .start();


                        var link = container.selectAll(".link")
                            .data(links).enter().append("g")
                            .attr("class", "link");

                        link.append("line")
                            .style("stroke-width", function(d) {
                                return d.arity * 2 - 1 + "px";
                            });

                        link.selectAll("line")
                            .style("stroke",function(d) {
                            return d.res != null ? color(d.res) : "#797979";

                        });

                        link.filter(function(d) { return d.arity > 1; }).append("line")
                            .style("stroke", "#fff")
                            .style("stroke-width","2px");

                        var node = container.selectAll(".node")
                            .data(nodes)
                            .enter().append("g")
                            .attr("class", function (d) {
                                return "node "+d.monName+d.res
                            })
                            .call(drag);

                        node.filter(function(d){ return d.name !="C"; }).append("circle")
                            .attr("r", 4)//6/4
                            .style("fill", function(d) {return "#FFFFFF";});//"return color(d.res)#E1ECFF //#FFFFFF
                        //.style("fill", function(d) {return "#797979";})
                        //.style("opacity", .6);



                        node.filter(function(d){ return d.name !="C"; }).append("text")
                            .attr("dy", ".35em")
                            .attr("text-anchor", "middle")
                            .text(function(d) {return d.name;})
                            .style("font","8px sans-serif")
                            .style("font-weight","bold")
                            .style("fill",function(d) {
                                return color(d.res)
                            });

                        node.filter(function(d){ return d.labelRes !="NA"; }).append("text")
                            .attr("dx", function (d) {
                                return d.labelRes == "end" ? "-10px" : "10px";
                            })
                            .attr("dy", ".35em")
                            .style("font","10px sans-serif")
                            .attr("text-anchor", function (d) { return d.labelRes;})
                            .style("fill", function(d) {
                                return color(d.res)
                            })
                            .text(function(d) { return d.monName; });

                        container.style("opacity", 1e-6)
                            .transition()
                            .duration(1000)
                            .style("opacity", 1);

                        function tick() {
                            link.selectAll("line")
                                .attr("x1", function(d) { return d.source.x; })
                                .attr("y1", function(d) { return d.source.y; })
                                .attr("x2", function(d) { return d.target.x; })
                                .attr("y2", function(d) { return d.target.y; });

                            node.attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

                            /*
                             svg.selectAll("path").data(groups).attr("d", groupPath).enter().insert("path", "g")
                             .attr("id", function (d,i) { return "path_" + i; })
                             .style("fill",function(d) { return color(d.key); })
                             .style("stroke", function(d) { return color(d.key); })
                             .style("stroke-width", 20)
                             .style("stroke-linejoin", "round")
                             .style("opacity", .2);

                             */

                        }



                        function dragstarted(d) {
                            d3.event.sourceEvent.stopPropagation();
                            d3.select(this).classed("dragging", true);
                        }
                        $("#downloadJson").click(function(e){
                            downloadObjectAsJson(results)
                        });

                        function zoomed() {
                            container.attr("transform",
                                "translate(" + zoom.translate() + ")" +
                                "scale(" + zoom.scale() + ")"
                            );
                        }

                        function interpolateZoom (translate, scale) {
                            var self = this;
                            return d3.transition().duration(350).tween("zoom", function () {
                                var iTranslate = d3.interpolate(zoom.translate(), translate),
                                    iScale = d3.interpolate(zoom.scale(), scale);
                                return function (t) {
                                    zoom
                                        .scale(iScale(t))
                                        .translate(iTranslate(t));
                                    zoomed();
                                };
                            });
                        }

                        function zoomClick() {
                            var clicked = d3.event.target,
                                direction = 1,
                                factor = 0.2,
                                target_zoom = 1,
                                center = [centerCoordinateX, centerCoordinateY],
                                extent = zoom.scaleExtent(),
                                translate = zoom.translate(),
                                translate0 = [],
                                l = [],
                                view = {x: translate[0], y: translate[1], k: zoom.scale()};

                            d3.event.preventDefault();
                            direction = (this.id === 'zoom_in') ? 1 : -1;
                            target_zoom = zoom.scale() * (1 + factor * direction);

                            if (target_zoom < extent[0] || target_zoom > extent[1]) { return false; }

                            translate0 = [(center[0] - view.x) / view.k, (center[1] - view.y) / view.k];
                            view.k = target_zoom;
                            l = [translate0[0] * view.k + view.x, translate0[1] * view.k + view.y];

                            view.x += center[0] - l[0];
                            view.y += center[1] - l[1];

                            interpolateZoom([view.x, view.y], view.k);
                        }


                        d3.selectAll('.btn-zoom').on('click', zoomClick);






                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        d3.select("svg").remove();
                        d3.select(".errorMessage").style("visibility","visible");
                    }

                });


            }



    });
    $("#downloadPng").click(function(e){

        var svg = $("svg")[0];
        //svg.setAttribute("width",700);
        //svg.setAttribute("height",500);
        var clientWidth=document.getElementsByTagName('svg')[0].parentNode.clientWidth;
        var clientHeight=document.getElementsByTagName('svg')[0].parentNode.clientHeight;
        var canvas = document.createElement('canvas');
        canvas.width = clientWidth;
        canvas.height = clientHeight;
        var ctx = canvas.getContext("2d");
        var data = (new XMLSerializer()).serializeToString(svg);


        var data2 =data.replace( "width=\"100%\"","width=\""+clientWidth+"\"").replace( "height=\"100%\"","height=\""+clientHeight+"\"");



        var img = new Image();
        var svgBlob = new Blob([data2], {type: 'image/svg+xml;charset=utf-8'});

        var url = URL.createObjectURL(svgBlob);

        img.onload = function () {
            ctx.drawImage(img, 0, 0);

            var imgURI = canvas
                .toDataURL('image/png')
                .replace('image/png', 'image/octet-stream');


            var downloadLink = document.createElement("a");
            downloadLink.href = imgURI;
            downloadLink.download =  $("#name").val()+".png";
            document.body.appendChild(downloadLink);
            downloadLink.click();
            document.body.removeChild(downloadLink);

        };

        img.src = url;



    });
    $("#downloadSvg").click(function(e){

        var svg = $("svg")[0];
        svg.setAttribute("xmlns", "http://www.w3.org/2000/svg");
        var data = (new XMLSerializer()).serializeToString(svg);
        var svgBlob = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});
        var url = URL.createObjectURL(svgBlob);

        var downloadLink = document.createElement("a");
        downloadLink.href = url;
        downloadLink.download = $("#name").val()+".svg";
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);

    });
    function downloadObjectAsJson(results){
        var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(results,null, '\t'));
        var downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute("href",     dataStr);
        downloadAnchorNode.setAttribute("download", $("#name").val()+ ".json");
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    }

});
